// Scroll Effects
$(function() {
    var btt = $("a.c3.back-top");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 80) {
            btt.addClass("visible");
        } else {
            btt.removeClass("visible");
        }
    });
});

// Anchor Smooth Scolling
$('a[href*=\\#]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 824);
    }
});
