
var galleryThumbs = new Swiper('.gallery-thumbs.c29', {
    spaceBetween: 0,
    slidesPerView: 5,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    centeredSlides: false,
    navigation: {
      nextEl: '.swiper-button-next.c29',
      prevEl: '.swiper-button-prev.c29',
    },
  });
  var galleryTop = new Swiper('.gallery-top.c29', {
    spaceBetween: 0,
    noSwiping: true,
    onlyExternal: true,
    thumbs: {
      swiper: galleryThumbs
    }
  });
