// Modal JS

var screen = $('.modal-outter.c13');
var close = $('.modal-close.c13');
var modal = $('.modal.container.c13');
var trigger = $('.modal-trigger.c13');


// Show Modal
$(trigger).click(function(event){
  var show = $(this).siblings(screen);
  $(show).fadeIn(200);
  event.preventDefault();
});

// Close Modal
$(screen, close).on("click", function(e){
    var className = e.target.className;
    $(screen, modal).fadeOut(200);
});

// Allow Content To Be Clickable
$(modal).click(function(event) {
  event.stopPropagation();
});
