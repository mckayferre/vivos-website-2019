var swiper = new Swiper('.swiper-container.c9', {
    slidesPerView: 4,
    spaceBetween: 20,
    speed: 800,
    parallax: true,

    navigation: {
    nextEl: '.swiper-button-next.c9',
    prevEl: '.swiper-button-prev.c9',
    },

    breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 40,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 10,
        }
      }


});
