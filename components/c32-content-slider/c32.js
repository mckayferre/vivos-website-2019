
var galleryThumbs = new Swiper('.gallery-thumbs.c32', {
    spaceBetween: 0,
    slidesPerView: 5,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    centeredSlides: false,
    allowTouchMove: false,
    navigation: {
      nextEl: '.swiper-button-next.c32',
      prevEl: '.swiper-button-prev.c32',
    },
  });
  var galleryTop = new Swiper('.gallery-top.c32', {
    spaceBetween: 0,
    noSwiping: true,
    onlyExternal: true,
    allowTouchMove: false,
    thumbs: {
      swiper: galleryThumbs
    }
  });



  // Modal JS

  var videoModal = $('.modal-outter.c32');
  var vContainer = $('.modal.container.c32');
  var launch = $('.modal-trigger.c32');


  // Show Modal
  $(launch).click(function(event){
    var show =  $(this).parents().find(videoModal);
    $(show).fadeIn(200);
    event.preventDefault();
  });

  // Close Modal
  $(videoModal).on("click", function(e){
      var className = e.target.className;
      $(videoModal, vContainer).fadeOut(200);
  });

  // Allow Content To Be Clickable
  $(modal).click(function(event) {
    event.stopPropagation();
  });



// Get Vido For Modal
