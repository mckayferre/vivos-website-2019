// Hero Slider
// https://idangero.us/swiper/demos/


var slides = $('.swiper-wrapper.c8 .swiper-slide.c8').length -1;
var swiper = new Swiper('.swiper-container.c8', {
  spaceBetween: 0,
  centeredSlides: true,
  loop: true,
  speed: 800,
  parallax: true,
  watchOverflow: true,

  autoplay: {
    delay: 10500,
    disableOnInteraction: false,
  },

  navigation: {
    nextEl: '.swiper-button-next.c8',
    prevEl: '.swiper-button-prev.c8',
  },

  pagination: {
    el: '.swiper-pagination.c8',
  },

  onReachBeginning: function(){
    $('.swiper-button-next.c8').show();
    $('.swiper-button-prev.c8').hide();
  },



});
